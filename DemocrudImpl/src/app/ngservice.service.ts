import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

class Course {
}

@Injectable({
  providedIn: 'root'
})
export class NgserviceService {

  constructor(private _http: HttpClient) { }



  fetchCourseListFromRemote(): Observable<any>{
    return this._http.get<any>('http://localhost:8090/courses');
  }
  addCourseToRemote(course: Course): Observable<any>{
    return this._http.post<any>('http://localhost:8090/courses',course);
  }
  updateCourseToRemote(course: Course): Observable<any>{
    return this._http.post<any>('http://localhost:8090/courses',course);
  }
  deleteCourseBdyIdFromRemote(id: number): Observable<any> {
    return this._http.delete<any>('http://localhost:8090/courses/' + id);
  }
  featchCourseBdyIdFromRemote(id: number): Observable<any> {
    return this._http.get<any>('http://localhost:8090/courses/' + id);
  }
}
